﻿using Stocksharing.Common.Interfaces;
using System.Collections.Generic;

namespace Stocksharing.Common.Models
{
    public class Email : IEmail
    {
        public IEnumerable<string> To { get; set; }

        public string Body { get; set; }

        public string From { get; set; }

        public string Subject { get; set; }
    }
}
