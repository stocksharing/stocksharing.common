﻿using System;

namespace Stocksharing.Common.Interfaces
{
    public interface IEmail : IMessage
    {
        /// <summary>
        /// От кого
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Тема сообщения
        /// </summary>
        public string Subject { get; set; }
    }
}
