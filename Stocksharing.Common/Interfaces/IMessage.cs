﻿using System.Collections.Generic;

namespace Stocksharing.Common.Interfaces
{
    public interface IMessage
    {
     
        /// <summary>
        /// Кому
        /// </summary>
        public IEnumerable<string> To { get; set; }

        /// <summary>
        /// Тело сообщения
        /// </summary>
        public string Body { get; set; }
    }
}
