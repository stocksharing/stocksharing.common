﻿using System.Threading.Tasks;

namespace Stocksharing.Common.Interfaces
{
    /// <summary>
    /// Обработчик для работы с Notifier'ом
    /// </summary>
    public interface INotifierHandler
    {
        /// <summary>
        /// Отправить Email-сообщение
        /// </summary>
        /// <param name="message">Email-сообщение</param>
        /// <returns></returns>
        Task SendEmailAsync(IEmail message);
    }
}
