﻿using Newtonsoft.Json;
using Stocksharing.Common.Interfaces;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Stocksharing.Common.Handlers
{
    public class NotifierHandler : INotifierHandler
    {
        private readonly HttpClient _client;

        public NotifierHandler(string baseUrl)
        {
            if (string.IsNullOrWhiteSpace(baseUrl))
                new ArgumentNullException(nameof(baseUrl));

            _client = new HttpClient
            {
                BaseAddress = new Uri(baseUrl),
            };
        }

        public async Task SendEmailAsync(IEmail message)
        {
            var data = new StringContent(JsonConvert.SerializeObject(message), Encoding.UTF8, "application/json");

            var response = await _client.PostAsync("api/notification/email", data);

            if (!response.IsSuccessStatusCode)
            {
                var contentJson = await response.Content.ReadAsStringAsync();
                var content = JsonConvert.DeserializeAnonymousType(contentJson, new { Message = "" });
                new WebException(content.Message);
            }
        }
    }
}
